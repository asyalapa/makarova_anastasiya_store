import {useMemo} from "react";

export const useSortedPosts = (products, sort) => {
    const sortedProducts = useMemo(() => {
        if(sort) {
            return [...products].sort((a, b) => a[sort].localeCompare(b[sort]))
        }
        return products;
    }, [sort, products])

    return sortedProducts;
}