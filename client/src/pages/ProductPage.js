import React, {useState} from 'react';

const ProductPage = () => {
    const [product, setProduct] = useState({info: []})

    return (
        <div className={'page__container'}>
            <div className={'page__container-info'}>
                <div className={'page__info-image'}>
                    <img className={'page__info-image-pic'}/>
                </div>
                <div className={'page__info-text'}>
                    <div className={'e'}>
                        <h2 className={'page__info-title'}>{product.name}</h2>
                        <div className={'page__info-rate'}>
                            {product.rating}
                        </div>
                    </div>
                </div>
                <div className={''}>
                    <div>
                        <h3>От: {product.price} руб.</h3>
                        <button>Добавить в корзину</button>
                    </div>
                </div>
            </div>
            <div className={'page__container-char'}>
                <h1>Характеристики</h1>
                {product.info.map((info, index) =>
                    <div key={info.id} style={{background: index % 2 === 0 ? 'lightgray' : 'transparent', padding: 10}}>
                        {info.title}: {info.description}
                    </div>
                )}
            </div>
        </div>
    );
};

export default ProductPage;