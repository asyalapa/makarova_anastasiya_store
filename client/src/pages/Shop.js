import React, {useContext, useState} from 'react';
import '../styles/App.css'
import CategoryBar from "../components/CategoryBar";
import SizeBar from "../components/SizeBar";
import ProductList from "../components/ProductList";
import {Context} from "../index";
import SortSelect from "../components/UI/SortSelect";

const Shop = () => {
    const [selectedSort, setSelectedSort] = useState('')
    const {product} = useContext(Context);
    const [products, setProducts] = useState(product.products)
    console.log(product.products)

    const sortProducts = (sort) => {
        setSelectedSort(sort);
        if (sort==='name') setProducts([...products].sort((a, b) => a[sort].localeCompare(b[sort])))
        else setProducts([...products].sort((a, b) => a[sort]-b[sort]))
    }

    return (
        <div className={'shop_wrapper'}>
            <section className="shop__product-sizes">
                <SortSelect value={selectedSort}
                            onChange={sortProducts}
                            defaultValue="Сортировка"
                            options={[
                                {value: 'name', name: 'По названию'},
                                {value: 'price', name: 'По цене'},
                            ]} />
                <SizeBar />
            </section>
            <CategoryBar />
            <ProductList product={products}/>
        </div>
    );
};

export default Shop;