import React, {useContext, useState} from 'react';
import '../styles/App.css'
import {Context} from "../index";

const Admin = () => {
    const [visibleSize, setVisibleSize] = useState(false)
    const [visibleCategory, setVisibleCategory] = useState(false)
    const [visibleProduct, setVisibleProduct] = useState(false)
    const {product} = useContext(Context)
    const [info, setInfo] = useState([])

    const addInfo = () => {
        setInfo([...info, {title: '', description: '', index: Date.now()}])
    }

    const removeInfo = (index) => {
        setInfo(info.filter(i => i.index !== index))
    }

    return (
        <div className={'admin__wrapper'}>
            <button className="admin__btn"
                    onClick={() => {visibleSize ? setVisibleSize(false) : setVisibleSize(true)}}
            >
                Добавить размер
            </button>
            <div className="admin__add_sizes"
                 style={visibleSize ? {display:'flex'} : {display: 'none'}}>
                <form action="" className="admin__form">
                    <input type="text" className="admin__form-input" placeholder={'Введите новый размер...'}/>
                    <input type={'submit'} className="admin__form-btn" value={'Добавить'}/>
                </form>
            </div>
            <button className="admin__btn"
                    onClick={() => {visibleCategory ? setVisibleCategory(false) : setVisibleCategory(true)}}
            >
                Добавить категорию
            </button>
            <div className="admin__add_category"
                 style={visibleCategory ? {display:'flex'} : {display: 'none'}}>
                <form action="" className="admin__form">
                    <input type="text" className="admin__form-input" placeholder={'Введите новую категорию... '}/>
                    <input type={'submit'} className="admin__form-btn" value={'Добавить'}/>
                </form>
            </div>
            <button className="admin__btn"
                    onClick={() => {visibleProduct ? setVisibleProduct(false) : setVisibleProduct(true)}}
            >
                Добавить товар
            </button>
            <div className="admin__add_product"
                 style={visibleProduct ? {display:'flex'} : {display: 'none'}}>
                <form action="" className="admin__form">
                    <select name="" id="" className="admin__form-select">
                        <optgroup label="Размер товара:">
                            {product.sizes.map(size =>
                                <option key={size.id}>
                                    {size.name}
                                </option>
                            )}
                        </optgroup>
                    </select>
                    <select name="" id="" className="admin__form-select">
                        <optgroup label="Категория товара:">
                            {product.categories.map(category =>
                                <option key={category.id}>
                                    {category.name}
                                </option>
                            )}
                        </optgroup>
                    </select>
                    <input type="text" className="admin__form-input" placeholder={'Введите название товара... '}/>
                    <input type="number" className="admin__form-input" placeholder={'Введите стоимость товара... '}/>
                    <input type="file" className="admin__form-file"/>
                    <input type="submit"
                           value={'Добавить характеристику'}
                           className="admin__form-btn"
                           onClick={(event) => {
                               event.preventDefault()

                               addInfo();
                           }} />
                    {info.map(i =>
                        <div className="admin__form-info" key={i.index}>
                            <input type="text" className="admin__form-input form-input-info" placeholder={'Введите название характеристики... '}/>
                            <input type="text" className="admin__form-input form-input-info" placeholder={'Введите описание характеристики... '}/>
                            <input type="submit"
                                   value={'Удалить'}
                                   className="admin__form-btn form-btn-info"
                                   onClick={(event) => {
                                       event.preventDefault()
                                       removeInfo(i.index)
                                   }} />
                        </div>
                    )}
                    <input type={'submit'} className="admin__form-btn" value={'Добавить'}/>
                </form>
            </div>
        </div>

    );
};

export default Admin;