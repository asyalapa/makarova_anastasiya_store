import React from 'react';
import '../styles/App.css'

const ProductFilter = ({filter, setFilter}) => {
    const sorting = [
        {value: 'name', name: 'По названию'},
        {value: 'price', name: 'По цене'}
    ]
    return (
        <div>
            <select className="shop__sort"
                value={filter.sort}
                onChange={selectedSort => setFilter({...filter, sort: selectedSort})} >
                <option defaultValue="Сортировка">
                    {sorting}
                </option>
            </select>
        </div>
    );
};

export default ProductFilter;