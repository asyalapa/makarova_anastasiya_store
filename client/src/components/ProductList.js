import React from 'react';
import '../styles/App.css'
import ProductCard from "./ProductCard";

const ProductList = ({product}) => {

    return (
        <div className="shop__card-list">

            {product.map(item =>
                <ProductCard key={item.id} product={item}/>
            )}
        </div>
    );
};

export default ProductList;