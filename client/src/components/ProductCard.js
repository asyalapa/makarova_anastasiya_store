import React from 'react';
import '../styles/App.css'
import {PRODUCT_ROUTE} from "../utils/consts";
import { useNavigate } from 'react-router-dom';
import picture from '../img/111.png'

const ProductCard = ({product}) => {
    const navigate = useNavigate();
    return (
        <div className="shop__card-items"
             style={{backgroundImage: `url(${picture})`}}
             onClick={() => navigate(PRODUCT_ROUTE + `/${product.id}`)}>
            <div className="shop__card-item">
                <h3 className={'shop__item-title'}>{product.name}</h3>
                <div>
                    <p className={'shop__item-rate'}>Рейтинг: {product.rating} / 5</p>
                </div>
                <p className={'shop__item-price'}>{product.price}</p>
                <button className="shop__item-button" onClick={(event) => event.stopPropagation()}>Добавить в корзину</button>
            </div>
        </div>
    );
};

export default ProductCard;